package alphadv.redomestic;

import alphadv.redomestic.EventRegister.EventRegister;
import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {


    PluginManager pm = this.getServer().getPluginManager();

    @Override
    public void onLoad() {
     this.getServer().broadcastMessage(ChatColor.DARK_GREEN+"------------------------------");
     this.getServer().broadcastMessage(ChatColor.GREEN+"Redomestic V-0.1");
     this.getServer().broadcastMessage(ChatColor.DARK_GREEN+"------------------------------");
    }



    @Override
    public void onEnable() {
        pm.registerEvents(new EventRegister(),this);
        super.onEnable();
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll();
        super.onDisable();
    }

}
